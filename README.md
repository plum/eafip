# Eafip
Eafip es una re-implementaci&oacute;n de la gema Bravo (https://github.com/leanucci/bravo#readme). La cual permite la obtención del C.A.E. (Código de Autorización Electrónico) por medio del Web Service de Facturación Electrónica provisto por AFIP.

## Modificaciones

Las modificaciones que se realizo fue el uso de Clases en lugar de Modulo.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'eafip'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install eafip

## Usage

Crear un archivo de configuracion en

  $ config/initializers/eafip.rb

con el siguiente codifo

```
require 'eafip'
Eafip.logger            = { log: true, level: :debug }
Eafip.openssl_bin       = '/usr/bin/openssl'
```

Para poder facturar de forma electronica se debe crear una instancia de `Eafip::Company` especificando los siguientes attributos

 * cuit: Cuit de la Empresa
 * pto_venta: Punto de Venta
 * documento: Tipo de documento del Cliente:    ['CUIT', 'DNI', 'Doc. (Otro)']
 * pkey: Path al archivo de la clave privada,
 * cert: Path al archivo del certificado
 * concepto: Concepto frente al Afip de la Empresa ['Productos', 'Servicios', 'Productos y Servicios']
 * moneda: :peso,
 * iva_cond: Condición del Iva frente al Afip [:responsable_inscripto, :responsable_monotributo, :exento]
 * environment: Ambiente de facturacion [:test, :production]
                                 }

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

