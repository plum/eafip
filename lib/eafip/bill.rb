# encoding: utf-8
module Eafip
  # The main class in Bravo. Handles WSFE method interactions.
  # Subsequent implementations will be added here (maybe).
  #
  class Bill
    # Returns the Savon::Client instance in charge of the interactions with WSFE API.
    # (built on init)
    #
    attr_reader :client

    attr_accessor :bill_type, :due_date, :date_from, :date_to, :body, :response, :invoice_type, :batch, :emission_at

    def initialize(company, attrs = {})
      @company = company
      opts = { wsdl: @company.auth_data.wsfe_url, ssl_version: :TLSv1_2 }.merge! Eafip.logger_options
      @client       ||= Savon.client(opts)
      @body           = { 'Auth' => @company.auth_data.auth_hash }
      @bill_type      = validate_bill_type(attrs[:bill_type])
      @invoice_type   = validate_invoice_type(attrs[:invoice_type])
      @batch          = attrs[:batch] || []
    end

    def inspect
      %{#<Eafip::Bill bill_type: "#{ bill_type }", emission_at: "#{ emission_at }", due_date: "#{ due_date }", date_from: #{ date_from.inspect }, \
date_to: #{ date_to.inspect }, invoice_type: #{ invoice_type }>}
    end

    def to_hash
      { bill_type: bill_type, invoice_type: invoice_type,
        due_date: due_date, date_from: date_from, date_to: date_to, body: body , emission_at: emission_at }
    end

    def to_yaml
      to_hash.to_yaml
    end

    # Searches the corresponding invoice type according to the combination of
    # the seller's IVA condition and the buyer's IVA condition
    # @return [String] the document type string
    #
    def bill_type_wsfe
      Eafip::BILL_TYPE[bill_type][invoice_type]
    end

    def set_new_invoice(invoice)
      if not invoice.instance_of?(Eafip::Bill::Invoice)
        raise(NullOrInvalidAttribute.new, "invoice debe ser del tipo Eafip::Bill::Invoice")
      end
      iva_condition = @company.iva_cond == :consumidor_final && invoice.total >= Eafip::PRICE_LIMIT ? :consumidor_final_1000 : @company.iva_cond
      bravo_type = Eafip::IVA_CONDITION[iva_condition][invoice.iva_condition][invoice_type]
      wsfe_type = bill_type_wsfe

      if bravo_type != wsfe_type
        raise(NullOrInvalidAttribute.new, "The invoice doesn't correspond to this bill type. #{bravo_type} != #{bill_type_wsfe}")
      end

      @batch << invoice if invoice.validate_invoice_attributes
    end

    # Files the authorization request to AFIP
    # @return [Boolean] wether the request succeeded or not
    #
    def authorize
      setup_bill

      response = client.call(:fecae_solicitar) do |soap|
        # soap.namespaces['xmlns'] = 'http://ar.gov.afip.dif.FEV1/'
        soap.message body
      end

      setup_response(response.to_hash)
      self.authorized?
    end

    # Sets up the request body for the authorisation
    # @return [Hash] returns the request body as a hash
    #
    def setup_bill
      fecaereq = setup_request_structure
      det_request = fecaereq['FeCAEReq']['FeDetReq']['FECAEDetRequest']
      last_cbte = @company.reference.next_bill_number(bill_type_wsfe)
      @batch.each_with_index do |invoice, index|
        cbte = last_cbte + index
        det_request << setup_invoice_structure(invoice, cbte)
      end
      body.merge!(fecaereq)
    end

    # Returns the result of the authorization operation
    # @return [Boolean] the response result
    #
    def authorized?
      !response.nil? && response.header_result == 'A' && invoices_result
    end

    private

    # Sets the header hash for the request
    # @return [Hash]
    #
    def header(bill_type)
      { 'CantReg' => "#{@batch.size}", 'CbteTipo' => bill_type, 'PtoVta' => @company.pto_venta }
    end

    def invoices_result
      response.detail_response.map{|invoice| invoice[:resultado] == 'A'}.all?
    end

    # Response parser. Only works for the authorize method
    # @return [Struct] a struct with key-value pairs with the response values
    #
    # rubocop:disable Metrics/MethodLength
    def setup_response(response)
      # TODO: turn this into an all-purpose Response class
      result          = response[:fecae_solicitar_response][:fecae_solicitar_result]

      unless result[:errors].blank?
        raise AfipError, "#{result[:errors][:err][:code]} - #{result[:errors][:err][:msg]}"
      end

      response_header = result[:fe_cab_resp]
      response_detail = result[:fe_det_resp][:fecae_det_response]

      # If there's only one invoice in the batch, put it in an array
      response_detail = response_detail.respond_to?(:to_ary) ? response_detail : [response_detail]

      response_hash = { header_result:   response_header[:resultado],
                        authorized_on:   response_header[:fch_proceso],
                        header_response: response_header,
                        detail_response: response_detail
                      }

      keys, values = response_hash.to_a.transpose

      self.response = Struct.new(*keys).new(*values)
    end
    # rubocop:enable Metrics/MethodLength

    def validate_invoice_type(type)
      if Eafip::BILL_TYPE_A.keys.include? type
        type
      else
        raise(NullOrInvalidAttribute.new, "invoice_type debe estar incluido en \
            #{ Eafip::BILL_TYPE_A.keys }")
      end
    end

    def setup_request_structure
      { 'FeCAEReq' =>
        { 'FeCabReq' => header(bill_type_wsfe),
          'FeDetReq' => {
            'FECAEDetRequest' => []
          } } }
    end

    def validate_bill_type(type)
      valid_types = Eafip::BILL_TYPE.keys
      if valid_types.include? type
        type
      else
        raise(NullOrInvalidAttribute.new,
          "El valor de iva_condition debe estar incluído en #{ valid_types }")
      end
    end

    def setup_invoice_structure(invoice, cbte)
      detail = {}
      detail['DocNro']    = invoice.document_number
      detail['ImpNeto']   = invoice.net_amount
      detail['ImpTotal']  = invoice.total
      detail['CbteDesde'] = detail['CbteHasta'] = cbte
      detail['Concepto']  = Eafip::CONCEPTOS[invoice.concept]
      detail['DocTipo']   = Eafip::DOCUMENTOS[invoice.document_type]
      detail['MonId']     = Eafip::MONEDAS[invoice.currency][:codigo]
    
      if invoice.company&.type_iva == 'ri'
        detail['ImpIVA'] = invoice.iva_sum
        unless invoice.list_ivas.blank?
          detail['Iva'] = {'AlicIva' => []}
          invoice.list_ivas.each do |eiva|
            detail['Iva']['AlicIva'] << {
              'Id' => eiva.code_iva,
              'BaseImp' => eiva.base_imp,
              'Importe' => eiva.amount
            }
          end
        end
      else
        detail['ImpIVA'] = 0
      end
    
      detail['CbteFch']    = emission(invoice) || today
      detail['ImpTotConc'] = 0.00
      detail['MonCotiz']   = 1
      detail['ImpOpEx']    = 0.00
      detail['ImpTrib']    = 0.00
    
      # Ajustar ImpTotal para que sea la suma de ImpNeto + ImpTrib solo si el tipo de IVA no es 'ri'
      unless invoice.company&.type_iva == 'ri'
        detail['ImpTotal'] = detail['ImpNeto'] + detail['ImpTrib']
      end
    
      # Agregar fechas si es necesario
      if send_dates?(invoice)
        detail.merge!('FchServDesde' => date_from || today,
                      'FchServHasta' => date_to || today,
                      'FchVtoPago'   => due_date || today)
      end
    
      detail
    end

    def today
      Time.new.strftime('%Y%m%d')
    end

    def emission(invoice)
      invoice.emission_at.present? ? invoice.emission_at.strftime('%Y%m%d') : nil
    end

    def send_dates?(invoice)
      [2, 3].include? Eafip::CONCEPTOS[invoice.concept].to_i
    end

    class Invoice
      attr_accessor :total, :document_type, :document_number, :due_date, :aliciva_id, :date_from, :date_to,
        :iva_condition, :concept, :currency, :list_ivas, :bill_type, :emission_at, :cbte_asocs, :period_asocs_from,
        :period_asocs_to

      attr_reader :net_amount, :iva_sum

      def initialize(company, attrs = {})
        @cbte_asocs     = attrs[:cbte_asocs] || []
        @period_asocs_from = attrs[:period_asocs_from] || ""
        @period_asocs_to = attrs[:period_asocs_to] || ""
        @document_number = attrs[:document_number] || ""
        @company = company
        @iva_condition  = validate_iva_condition(attrs[:iva_condition])
        @bill_type      = validate_bill_type(attrs[:bill_type])
        @list_ivas      = validate_list_ivas(attrs[:list_ivas])
        @total          = attrs[:total].round(2)|| 0.0
        @document_type  = attrs[:document_type] || @company.documento
        @currency       = attrs[:currency]      || @company.moneda
        @concept        = attrs[:concept]       || @company.concepto
        @iva_sum        = calculate_iva_sum
        @net_amount     = (@total - @iva_sum).round(2)
      end

      def validate_iva_condition(iva_cond)
        valid_conditions = Eafip::IVA_CONDITION[@company.iva_cond].keys
        if valid_conditions.include? iva_cond
          iva_cond
        else
          raise(NullOrInvalidAttribute.new,
            "El valor de iva_condition debe estar incluído en #{ valid_conditions }")
        end
      end

      def validate_bill_type(bill_type)
        unless Eafip::BILL_TYPE.keys.include? bill_type
          raise(NullOrInvalidAttribute.new,
            "El tipo de documento (Letra) debe estar incluído en #{ Eafip::BILL_TYPE.keys }")
        end
        bill_type
      end

      def validate_list_ivas(list_ivas)
        if list_ivas.respond_to? :each
          list_ivas.each do |eiva|
            raise(NullOrInvalidAttribute.new,
              "El elemento debe ser del Tipo Eafip::Eiva, #{eiva.class.name}") unless eiva.is_a? Eafip::Eiva
          end
        else
          raise(NullOrInvalidAttribute.new,
            "El listado de ivas (list_ivas) debe ser un elemento iterable")
        end
        list_ivas
      end

      def validate_invoice_attributes
        return true unless document_number.blank?
        raise(NullOrInvalidAttribute.new, "document_number debe estar presente.")
      end

      def document_c?
        @bill_type == :bill_c
      end

      def calculate_iva_sum
        (list_ivas.inject(0){ |sum, eiva| sum + eiva.amount }).round(2)
      end
    end
  end
end
