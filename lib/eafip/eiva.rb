module Eafip

  class Eiva
    attr_accessor :base_imp, :key_iva, :amount

    def initialize(attrs)
      self.base_imp = validate_price(attrs[:base_imp])
      self.key_iva = validate_key_iva(attrs[:key_iva])
      self.amount = validate_price(attrs[:amount])
    end

    def code_iva
      Eafip::ALIC_IVA[key_iva][0]
    end

    private

    def validate_price(value)
      float_value = Float(value)
      raise(NullOrInvalidAttribute.new,
          "El Monto ingresaso es invalido, debe ser numérico y mayor o igual a cero") if float_value < 0.0
      float_value.round(2)
    end

    def validate_key_iva(type)
      valid_types = Eafip::ALIC_IVA.keys
      if valid_types.include? type
        type
      else
        raise(NullOrInvalidAttribute.new,
          "El código del iva debe estar incluído en #{ valid_types }")
      end
    end

  end
end

